package be.kdg.model;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

//TODO: klaarmaken voor JAXB
public class Piloten {
    private List<Piloot> myList;

    // Default constructor verplicht aanwezig!
    public Piloten() {
        myList = new ArrayList<>();
        myList.add(new Piloot("Mercedes", "Hamilton", 44, LocalDate.of(2018, 10, 12)));
        myList.add(new Piloot("Mercedes", "Rosberg", 6, LocalDate.of(2017, 4, 1)));
        myList.add(new Piloot("Ferrari", "Vettel", 5, LocalDate.of(2018, 7, 27)));
        myList.add(new Piloot("Ferrari", "Räikkönen", 7, LocalDate.now()));
        myList.add(new Piloot("McLaren", "Bottas", 77, LocalDate.of(2018, 9, 5)));
        myList.add(new Piloot("McLaren", "Massa", 19, LocalDate.now()));
        myList.add(new Piloot("Red Bull", "Ricciarddo", 3, LocalDate.of(2016, 11, 30)));
        myList.add(new Piloot("Red Bull", "Kvyat", 26, LocalDate.now()));
    }

    public List<Piloot> getPiloten() {
        return myList;
    }
}
